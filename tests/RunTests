#!/bin/sh
#
# Run some regression test cases.
#
# If there is a .lst.ok file, it compares the listing.
# If there is a .objd.ok file, it compares the result of dumpobj.
#

TESTS="test-asciz \
    test-backpatch \
    test-blkb \
    test-bsl-mac-arg \
    test-cis \
    test-complex-reloc \
    test-enabl-ama \
    test-enabl-lcm \
    test-endm \
    test-float \
    test-gbl \
    test-if \
    test-impword \
    test-include \
    test-jmp \
    test-labels \
    test-listing \
    test-locals \
    test-macro-comma \
    test-mcall-file \
    test-opcodes \
    test-operands \
    test-packed \
    test-prec \
    test-psect \
    test-rad50 \
    test-radix \
    test-reg \
    test-reloc \
    test-rept \
    test-syntax \
    test-ua-pl \
    test-undef \
    test-word-comma"

status=0

assemble() {
    local opts="$1"
    local t="$2"

    local out=$( echo ${t} | sed -e 's;/;-;g' -e 's;\.m11$;;' -e 's;\.mac$;;' )

    ../macro11 -e BMK,ME -dc BEX,ME,MEB ${opts} -l "$out".lst -o "$out".obj "$t" 2>/dev/null

    # Ignore error status and messages from the assembler.
    # We check the listing file to see what we expect.

    if [ -e "$out".lst.ok ]
    then
        diff -u "$out".lst.ok "$out".lst
        status=$((status + $?))
    fi

    if [ -e "$out".objd.ok ]
    then
        ../dumpobj $fmt "$out".obj >"$out".objd
        diff -u "$out".objd.ok "$out".objd
        status=$((status + $?))
    fi
}

assemble_both() {
    local opts="$1"
    local file="$2"

    assemble "-rsx  ${opts}" "${file}"
#    assemble "-rt11 ${opts}" "${file}"
}

accept() {
    for t in $*
    do
        cp "$t" "$t.ok"
        git add "$t.ok"
    done
}


for t in $TESTS
do
    assemble_both -stringent "${t}.mac"
done

for t in 2.11BSD/m11/*.m11
do
    assemble_both "-strict -ym11 -s debug=1 ./2.11BSD/m11/at.sml" "$t"
done

exit $status
